# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.i18n import gettext
from trytond.model import fields
from trytond.pool import PoolMeta

from .exceptions import MissingPermissionPosAccess


class User(metaclass=PoolMeta):
    __name__ = 'res.user'
    # frontend_admin is just one can delete sales
    type_pos_user = fields.Selection([
            ('', ''),
            ('order', 'Order User'),
            ('cashier', 'Cashier User'),
            ('salesman', 'Salesman'),
            ('frontend', 'Frontend User'),
            ('frontend_admin', 'Frontend Admin'),
        ], 'Type POS User')

    @classmethod
    def __setup__(cls):
        super(User, cls).__setup__()
        cls._preferences_fields.append('sale_device')
        cls._preferences_fields.append('type_pos_user')
        cls._context_fields.insert(0, 'sale_device')
        cls._context_fields.insert(0, 'type_pos_user')

    def _check_pos_access(self, required_access):
        current_access = []
        for group in self.groups:
            current_access.extend([ma.model.model for ma in group.model_access if ma.perm_create])
        missing_access = set.difference(set(required_access), set(current_access))
        if not missing_access:
            return self.type_pos_user
        raise MissingPermissionPosAccess(
            gettext('sale_pos_frontend.msg_missing_access', s=list(missing_access)))
        return ''

    @fields.depends('type_pos_user', 'groups')
    def on_change_with_type_pos_user(self):
        if not self.type_pos_user:
            return ''

        required_access = ['sale.sale']
        if self.type_pos_user in ['frontend_admin', 'frontend', 'cashier']:
            required_access.extend([
                'account.move',
                'account.invoice',
                'stock.move',
                'account.statement',
            ])
        return self._check_pos_access(required_access)


class UserApplication(metaclass=PoolMeta):
    __name__ = 'res.user.application'

    @classmethod
    def __setup__(cls):
        super(UserApplication, cls).__setup__()
        cls.application.selection.append(('system_pos', 'POS System'))

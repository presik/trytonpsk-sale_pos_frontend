# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date, datetime
from decimal import Decimal
from operator import attrgetter

from sql import Table
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import (
    Button,
    StateAction,
    StateReport,
    StateTransition,
    StateView,
    Wizard,
)

from .exceptions import AddProductError, DataSaleError

_ZERO = Decimal('0.0')

STATES = {
    'readonly': Eval('state').in_(['processing', 'done', 'cancel']),
}

STATES_DELIVERY = [
    ('', ''),
    ('to_dispatch', 'To Dispatch'),
    ('dispatched', 'Dispatched'),
    ('delivered', 'Delivered'),
    ('canceled', 'Canceled'),
]


OPTIONS_STATUS = [
    ('', ''),
    ('draft', 'Draft'),
    ('requested', 'Requested'),
    ('commanded', 'Commanded'),
    ('in_preparation', 'In Preparation'),
    ('dispatched', 'Dispatched'),
    ('delivered', 'Delivered'),
    ('rejected', 'Rejected'),
    ('cancelled', 'Cancelled'),
]


def _round(self, value, digits=2):
    return Decimal(str(round(float(value)), digits))


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    cash_received = fields.Numeric('Cash Received', digits=(16, 2),
                                   states=STATES)
    turn = fields.Integer('Turn', states=STATES, depends=['state'])
    delivery = fields.Char('Delivery', states=STATES)
    change = fields.Numeric('Change', states=STATES)
    delivery_party = fields.Many2One('sale.delivery_party', 'Delivery Party',
                                     states=STATES)
    tip = fields.Numeric('Tip', digits=(
        16, 2), states=STATES, depends=['state'])
    pos_notes = fields.Char('POS Notes', states={
        'readonly': Eval('state') != 'draft',
    })
    delivery_charge = fields.Selection([
        ('', ''),
        ('customer', 'Customer'),
        ('company', 'Company'),
        ], 'Delivery Charge', states={'invisible': Eval('kind') != 'delivery'})
    delivery_charge_string = delivery_charge.translated('delivery_charge')
    invoice = fields.Many2One('account.invoice', 'Invoice')
    delivery_state = fields.Selection(STATES_DELIVERY, 'Delivery State', states={'invisible': Eval('kind') != 'delivery'})
    delivery_time = fields.Time('Delivery Time', format='%H:%M', states={'invisible': Eval('kind') != 'delivery'})
    tip_amount = fields.Numeric('Tip Amount', digits=(16, 2), states=STATES,
        depends=['state'])
    delivery_amount = fields.Numeric('Delivery Amount', digits=(16, 2),
        states=STATES, depends=['state'])
    additional_paid_amount = fields.Numeric('Additional Paid Amount', digits=(16, 2))
    net_amount = fields.Function(fields.Numeric('Net Amount', digits=(16, 2)),
        'get_net_amount')
    payment_method = fields.Selection([
        ('', ''),
        ('terminal', 'Terminal'),
        ('cash', 'Cash'),
        ('all_paid', 'All Paid'),
        ('partial_paid', 'Partial Paid'),
        ('gateway', 'Gateway'),
    ], 'Payment Method')
    payment_method_string = payment_method.translated('payment_method')
    kind = fields.Selection([
        ('', ''),
        ('delivery', 'Delivery'),
        ('take_away', 'Take Away'),
        ('to_table', 'To Table'),
        ('catering', 'Catering'),
    ], 'Kind Order')
    kind_string = kind.translated('kind')
    courtesy = fields.Boolean('Courtesy')
    order_status = fields.Selection(OPTIONS_STATUS, 'Order Status')
    order_status_string = order_status.translated('order_status')
    waiting_time = fields.Function(fields.Integer('Shipping Time',
        help="In Minutes"), 'get_waiting_time')
    order_status_time = fields.Many2One('sale.order_status.time',
        string='Times Order Status')
    consumer = fields.Many2One('party.consumer', 'Consumer')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        if table_h.column_exist('delivery_way'):
            table_h.drop_column('delivery_way')

        if table_h.column_exist('delivery_time') and table_h.column_is_type('delivery_time', 'NUMERIC'):
            table_h.drop_column('delivery_time')

        if table_h.column_exist('shipping_time'):
            table_h.drop_column('shipping_time')
        super(Sale, cls).__register__(module_name)

    @staticmethod
    def default_sale_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_tip_amount():
        return 0

    @staticmethod
    def default_delivery_amount():
        return 0

    @staticmethod
    def default_additional_paid_amount():
        return 0

    @staticmethod
    def default_self_pick_up():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if not user or not user.shop:
            return True
        return user.shop.self_pick_up

    @staticmethod
    def default_invoice_address():
        pool = Pool()
        Party = pool.get('party.party')
        Shop = pool.get('sale.shop')
        shop_id = Transaction().context.get('shop')
        if shop_id is None:
            return
        shop = Shop(shop_id)
        if shop and shop.party:
            invoice_address = Party.address_get(shop.party, type='invoice')
            return invoice_address.id

    @staticmethod
    def default_shipment_address():
        pool = Pool()
        Party = pool.get('party.party')
        Shop = pool.get('sale.shop')
        shop_id = Transaction().context.get('shop')
        if shop_id is None:
            return

        shop = Shop(shop_id)
        if shop and shop.party:
            shipment_address = Party.address_get(shop.party, type='delivery')
            return shipment_address.id

    @staticmethod
    def default_order_status():
        return 'draft'

    def get_waiting_time(self, name=None):
        now = datetime.now()
        if self.state in ('draft', 'quotation', 'confirmed', 'processing'):
            delta = int((now - self.create_date).seconds / 60)
            return delta

    @classmethod
    def get_credit_amount_party(cls, args, context=None):
        Party = Pool().get('party.party')
        party = Party(args['party_id'])
        return party.receivable or 0

    @classmethod
    def get_paid_amount(cls, sales, names):
        result = super(Sale, cls).get_paid_amount(sales, names)
        for name in names:
            for sale in sales:
                result[name][sale.id] += sale.additional_paid_amount or 0
                # for payment in sale.payments:
                #     result[name][sale.id] += payment.extra_amount if payment.extra_amount else 0
        return result

    def get_residual_amount(self, name=None):
        residual = super(Sale, self).get_residual_amount(name)
        residual += (self.tip_amount or 0) + (self.delivery_amount or 0)
        return residual

    @classmethod
    def copy(cls, sales, default=None):
        if default is None:
            default = {}
        default['cash_received'] = None
        default['change'] = None
        default['order_status'] = 'draft'
        default['reference'] = None
        return super(Sale, cls).copy(sales, default)

    def get_net_amount(self, name=None):
        return sum([self.untaxed_amount, self.tax_amount, self.tip_amount or 0,
                    self.delivery_amount or 0])

    @classmethod
    def draft(cls, sales):
        super(Sale, cls).draft(sales)
        for sale in sales:
            cls._delete_stock_moves(sale)

    @classmethod
    def get_reversion(cls, args, context={}):
        sale = cls(args['sale_id'])
        line_id = args.get('sale_line_id')
        products = {}
        for line in sale.lines:
            if line.order_sended and line.id == line_id:
                products = {line.product: (
                    line.product, line.quantity, 0, line.note, line.id)}
                break
        orders = cls._get_products_order(sale, products)
        return orders, sale.number

    @classmethod
    def dispatch_delivery(cls, args):
        sale_ids = args.get('sales')
        delivery = args.get('delivery_party')
        sales = cls.browse(sale_ids)
        cls.write(sales, {'order_status': 'dispatched', 'delivery_party': delivery})
        if args.get('dispatched'):
            pool = Pool()
            StatusTime = pool.get('sale.order_status.time')
            orders = StatusTime.search([('sale', 'in', sale_ids)])
            try:
                StatusTime.write(orders, {'dispatched': args.get('dispatched')})
            except Exception:
                StatusTime.write(orders, {'dispatched': datetime.now()})

    @classmethod
    def confirm_delivery(cls, args):
        sale_ids = args.get('sales')
        sales = cls.browse(sale_ids)
        cls.write(sales, {'order_status': 'delivered'})
        pool = Pool()
        StatusTime = pool.get('sale.order_status.time')
        orders = StatusTime.search([('sale', 'in', sale_ids)])
        StatusTime.write(orders, {'delivered': datetime.now()})

    @classmethod
    def get_order2print(cls, args, context=None):
        Line = Pool().get('sale.line')
        sale = cls(args['sale_id'])
        if not sale.number:
            cls.set_number([sale])
        if not sale.sale_device:
            User = Pool().get('res.user')
            context = Transaction().context
            user = User(context.get('user'))
            sale_device = context.get('sale_device') or user.sale_device
            if sale_device:
                sale.sale_device = sale_device
                sale.save()

        # cls.store_cache([sale])
        # Set context for printing instance
        lines_mark_sended = []
        products2order = {}
        repeat = args.get('repeat')
        create_uid = None
        for line in sale.lines:
            if not line.product.template.printers:
                lines_mark_sended.append(line)
            if line.type != 'line' or (line.order_sended and not repeat):
                continue
            if not create_uid:
                create_uid = line.create_uid.rec_name
            products2order[line] = [
                line.product, line.quantity, line.unit_price_w_tax, line.note, line.id, line.parent,
            ]
        Line.write(lines_mark_sended, {'order_sended': True})
        order_by_products = False
        if (sale.sale_device and sale.sale_device.environment == 'restaurant'):
            order_by_products = True
        if order_by_products:
            orders = cls._get_products_order(sale, products2order, create_uid)
        else:
            orders = cls._get_order(sale)
        return orders, sale.number

    def _get_info_consumer(self):
        consumer = {}
        if hasattr(self, 'consumer') and self.consumer:
            consumer['consumer_name'] = self.consumer.name
            consumer['consumer_phone'] = self.consumer.phone
            consumer['consumer_address'] = self.consumer.address
            consumer['consumer_notes'] = self.consumer.notes or ''
        return consumer

    @classmethod
    def _get_order(cls, sale):
        PosPrinter = Pool().get('sale.pos_printer')
        orders = {}
        printers = PosPrinter.search([])
        agent = ''
        if sale.agent:
            agent = sale.agent.party.name

        if printers:
            printer = printers[0]
            printer_id = str(printer.id)
        else:
            printer = None
            printer_id = 'None'

        number = ''
        if sale.invoice_number or (sale.invoice and sale.invoice.number):
            number = sale.invoice_number or (
                sale.invoice and sale.invoice.number)

        consumer = sale._get_info_consumer()
        orders[printer_id] = {
            'interface': printer.interface if printer else None,
            'host': printer.host if printer else None,
            'row_characters': printer.row_characters if printer else 48,
            'turn': sale.turn or '',
            'number': number,
            'sale_number': sale.number or '',
            'position': sale.position or '',
            'party': sale.party.name,
            'kind': sale.kind,
            'delivery_amount': sale.delivery_amount,
            'agent': agent,
            'consumer': consumer,
            'comment': sale.comment or '',
            'payment_term': sale.payment_term.name if sale.payment_term else '',
            'delivery_charge': sale.delivery_charge,
            'total_amount': sale.total_amount_cache or str(sale.total_amount),
            'shop': sale.shop.name,
            'lines': [],
            'lines_ids': [ln.id for ln in sale.lines],
        }
        _lines = []
        for l in sale.lines:
            _lines.append({
                'name': l.product.name,
                'quantity': str(l.quantity),
                'unit_price': '',
                'note': '',
            })
        orders[printer_id]['lines'] = _lines
        if hasattr(sale, 'table_assigned'):
            orders[printer_id]['table_assigned'] = sale.table_assigned.name if sale.table_assigned else ''

        extras = cls.get_extras(sale)
        if extras:
            orders[printer_id].update(extras)
        return orders

    @classmethod
    def get_extras(cls, sale):
        return {}

    @classmethod
    def _get_products_order(cls, sale, products, create_uid):
        orders = {}
        fields = [
            'shop.id', 'sale_device', 'party.name', 'position', 'invoice_number', 'number', 'turn',
            'delivery_charge', 'comment', 'payment_term', 'shop.name',
        ]
        shop_id, sale_device, party_name, position, invoice_number, number, turn, delivery_charge, comment, payment_term, shop_name = attrgetter(*fields)(sale)
        consumer = sale._get_info_consumer()
        for (product, qty, unit_price, notes, id, parent) in products.values():
            template = product.template
            for printer in template.printers:
                # shop_id = sale.shop.id
                if printer.shop.id != shop_id:
                    continue

                device = printer.sale_device
                printer_id = str(printer.id)
                if device:
                    if device and device == sale_device:
                        printer_id += str(sale_device.id)
                    else:
                        continue

                if printer_id not in orders:
                    agent = ''
                    if sale.agent:
                        agent = sale.agent.party.name
                    orders[printer_id] = {
                        'interface': printer.interface,
                        'host': printer.host,
                        'row_characters': printer.row_characters or 48,
                        'turn': turn or '',
                        'sale_number': number or '',
                        'number': invoice_number or '',
                        'position': position or '',
                        'party': party_name,
                        'agent': agent,
                        'consumer': consumer,
                        'comment': comment or '',
                        'delivery_charge': delivery_charge,
                        'payment_term': payment_term.name if payment_term else '',
                        'total_amount': sale.total_amount_cache or str(sale.total_amount),
                        'shop': shop_name,
                        'create_uid': create_uid,
                        'lines_ids': [],
                    }
                    if printer.categories:
                        orders[printer_id]['lines'] = {c.sequence: {'name': c.category.name, 'lines': []} for c in printer.categories}
                        orders[printer_id]['categories'] = {c.category.id: c.sequence for c in printer.categories}
                    else:
                        orders[printer_id]['lines'] = []
                    if hasattr(sale, 'kind'):
                        orders[printer_id]['kind'] = sale.kind or ','
                    if hasattr(sale, 'table_assigned'):
                        orders[printer_id]['table_assigned'] = sale.table_assigned.name if sale.table_assigned else ''
                    extras = cls.get_extras(sale)
                    if extras:
                        orders[printer_id].update(extras)
                value_line = {
                    'name': template.name,
                    'quantity': str(qty),
                    'unit_price': int(unit_price),
                    'note': notes,
                    'parent': parent,
                }
                if isinstance(orders[printer_id]['lines'], list):
                    orders[printer_id]['lines'].append(value_line)
                    orders[printer_id]['lines_ids'].append(id)
                else:
                    try:
                        key_id = orders[printer_id]['categories'][template.categories[0].id]
                    except Exception:
                        if 'others' not in orders[printer_id]['lines']:
                            orders[printer_id]['lines']['others'] = {'name': 'OTROS', 'lines': []}
                        key_id = 'others'
                    try:
                        orders[printer_id]['lines'][key_id]['lines'].append(value_line)
                        orders[printer_id]['lines_ids'].append(id)
                    except Exception:
                        orders[printer_id]['lines'][key_id]['lines'] = [value_line]
                        orders[printer_id]['lines_ids'].append(id)
        return orders

    def _updated_amounts(self):
        total_amount = self.untaxed_amount + self.tax_amount
        data_send = {
            'untaxed_amount': self.untaxed_amount,
            'tax_amount': self.tax_amount,
            'total_amount': total_amount,
            'net_amount': self.net_amount,
            'residual_amount': self.residual_amount,
            'paid_amount': self.paid_amount,
            'change': self.change,
        }
        return data_send

    @classmethod
    def process_sale(cls, sale):
        # Validate and Process sale before add payments
        # return error message if occurs or
        # invoice number is all right
        msg = ''
        if sale.payment_term.id != sale.shop.payment_term.id and \
                hasattr(sale.party, 'credit_limit_amount') and sale.party.credit_limit_amount:
            if (sale.total_amount + sale.party.credit_amount) > sale.party.credit_limit_amount:
                return {
                    'res': 'error',
                    'msg': 'credit_limit_exceed',
                }

        if sale.state not in ['processing', 'done']:
            sale = cls.process_sale_pos(sale)
        if sale.invoice:
            msg = sale.invoice.number
        return {
            'res': 'ok',
            'msg': msg,
        }

    @classmethod
    def faster_process(cls, args, context=None):
        # Validate and Process sale before add payments
        # return error message if occurs or invoice number if all is right
        Invoice = Pool().get('account.invoice')
        sale_id = args.get('sale_id')
        if not sale_id:
            sale_id = args.get('id')

        sale, = cls.browse([sale_id])
        if not sale.number:
            cls.set_number([sale])
        msg = None
        exception = None
        res = {}
        if sale.payment_term.id != sale.shop.payment_term.id and \
                hasattr(sale.party, 'credit_limit_amount') and sale.party.credit_limit_amount:
            if (sale.total_amount + sale.party.credit_amount) > sale.party.credit_limit_amount:
                msg = 'credit_limit_exceed'

        if sale.state not in ['processing', 'done']:
            sale = cls.process_sale_pos(sale)
        if sale.invoice:
            invoice = sale.invoice
            if invoice.invoice_type not in ('P', 'M', 'C'):
                exception = Invoice.validate_invoice([invoice])
                if exception:
                    return res, msg, exception
        res = {
            'id': sale_id,
            'number': sale.number,
            'invoice_number': sale.invoice_number,
            'untaxed_amount': sale.untaxed_amount,
            'tax_amount': sale.tax_amount,
            'total_amount': sale.total_amount,
            'tip_amount': sale.tip_amount,
            'net_amount': sale.net_amount,
            'delivery_amount': sale.delivery_amount,
            'waiting_time': sale.waiting_time if hasattr(sale, 'waiting_time') else None,
            'sale_date': sale.sale_date,
            'payment_method': sale.payment_method,
            'order_status': sale.order_status if hasattr(sale, 'order_status') else None,
            'state': sale.state,
        }
        if hasattr(sale, 'consumer') and sale.consumer:
            res['consumer'] = {
                'id': sale.consumer.id,
                'name': sale.consumer.rec_name,
            }

        return res, msg, exception

    @classmethod
    def get_amounts(cls, args, context=None):
        sale, = cls.browse([args['sale_id']])
        return sale._updated_amounts()

    @classmethod
    def get_printing_context(cls, args, context=None):
        # Set context for printing instance
        pool = Pool()
        Device = pool.get('sale.device')
        User = pool.get('res.user')
        configuration = pool.get('sale.configuration')(1)
        # user = args.get('user')
        # user_id = args.get('user_id', None)
        user_id = Transaction().context.get('user')
        device = None
        if args.get('device_id'):
            device = Device(args['device_id'])
        user_name = ''
        if user_id:
            user = User(user_id)
            user_name = user.name
            if not device and user.sale_device:
                device = user.sale_device

        company, shop = device.shop.company, device.shop

        regime_tax = ''
        id_number = ''
        city_name = ''
        address = company.party.addresses[0]
        if device.shop.warehouse.address:
            address = device.shop.warehouse.address
        street = address.street
        city_name = address.city_co.name

        if company.party.id_number:
            id_number = str(company.party.id_number)
            if hasattr(company.party, 'id_number_full'):
                id_number = company.party.id_number_full

        if hasattr(company.party, 'regime_tax_string'):
            regime_tax = company.party.regime_tax_string

        printing_context = {
            'company': company.rec_name,
            'sale_device': device.name,
            'shop': shop.name,
            'user': user_name,
            'street': street,
            'city': city_name,
            'phone': company.party.phone or '',
            'logo': company.logo or '',
            'id_number': id_number,
            'regime_tax': regime_tax,
            'authorizations': shop.get_authorizations(),
            'show_position': shop.show_position,
            'show_discount': configuration.show_discount_pos,
            'print_lines_product': configuration.print_lines_product,
            'no_remove_commanded': configuration.no_remove_commanded,
            'header': shop.ticket_header,
            'footer': shop.ticket_footer,
            'order_copies': shop.order_copies,
            'invoice_copies': shop.invoice_copies,
        }
        if hasattr(shop, 'local_printer'):
            printing_context['local_printer'] = shop.local_printer
        return printing_context

    @classmethod
    def get_sale_from_invoice(cls, args, context={}):
        Invoice = Pool().get('account.invoice')
        number = args.get('number')
        res = False
        invoices = Invoice.search([
            ('number', '=', number),
        ])
        if invoices:
            sale = invoices[0].sales[0]
            res = {
                'id': sale.id,
                'invoices': [invoices[0].id],
                'invoice_type': sale.invoice_type,
            }
        return res

    @classmethod
    def add_tax(cls, args, context={}):
        sale_id = args['id']
        tax_id = args['tax_id']
        sale = cls(sale_id)
        for line in sale.lines:
            line.write([line], {'taxes': [('add', [tax_id])]})
        sale.save()
        return sale._updated_amounts()

    @classmethod
    def fmtdate(cls, raw_date):
        res = raw_date.strftime('%d/%m/%Y') if raw_date else ''
        return res

    @classmethod
    def fmttime(cls, raw_date):
        res = raw_date.strftime('%H:%M %p') if raw_date else ''
        return res

    @classmethod
    def fmtdatetime(cls, raw_date):
        res = raw_date.strftime('%d/%m/%Y %H:%M %p') if raw_date else ''
        return res

    @classmethod
    def set_courtesy(cls, args):
        sale, = cls.browse([args.get('id')])
        cls.do_stock_moves([sale])
        sale.invoice_type = 'M'
        sale.state = 'done'
        sale.invoice_method = 'manual'
        sale.courtesy = True
        sale.untaxed_amount_cache = None
        sale.tax_amount_cache = None
        sale.total_amount_cache = None
        lines = []
        for line in sale.lines:
            line.unit_price = 0
            lines.append(line)
        sale.lines = lines
        sale.on_change_lines()
        sale.save()
        cls.store_cache([sale])
        if hasattr(sale, 'table_assigned') and sale.table_assigned:
            ResTable = Pool().get('sale.shop.table')
            to_drop = {'state': 'available', 'sale': None}
            ResTable.write([sale.table_assigned], to_drop)
        return {
            'number': sale.number,
            'state': sale.state,
            'total_amount': sale.total_amount}

    @classmethod
    def get_data(cls, args, context=None):
        # FIXME all information must returns from invoice not from sales
        config = Pool().get('sale.configuration')(1)
        sale_id = args['sale_id']
        type_doc = args.get('type_doc', 'order')
        sale, = cls.browse([sale_id])
        if not sale.number:
            cls.set_number([sale])
        short_invoice = False
        company_ = sale.company
        invoice = None
        salesman = ''
        number = ''
        payment_term = ''
        sale_date = sale.sale_date
        # tip_product_id = None
        party_address = None
        party_phone = None
        party = sale.party
        delivery_party = ''
        qr_code = ''
        cufe = ''
        auth_kind = None
        sale_tip = ''
        if type_doc == 'invoice':
            short_invoice = config.print_invoice_short
            if not sale.invoices:
                raise DataSaleError(gettext('sale_pos_frontend.msg_sale_without_invoice'))
            else:
                invoice = sale.invoices[0]
            number = invoice.number
            payment_term = invoice.payment_term.name
            sale_date = invoice.invoice_date
            create_date = company_.convert_timezone(invoice.create_date)
            sale_time = cls.fmttime(create_date)
            if hasattr(invoice, 'cufe'):
                cufe = invoice.cufe
                if not cufe and invoice.shop and invoice.shop.cron_submit_invoice_dian:
                    cufe = invoice.get_cufe()
                if cufe:
                    qr_code = invoice.provider_link

            if invoice.authorization:
                auth_kind = invoice.authorization.kind
        else:
            create_date = company_.convert_timezone(sale.create_date)
            sale_time = cls.fmttime(create_date)
            sale_tip = sale.tip
        sale_date = cls.fmtdate(sale_date)
        create_date = cls.fmtdatetime(create_date)

        _lines = []

        reference_in_receipt = config.print_reference_receipt

        for line in sale.lines:

            code = line.product.code
            reference = None

            if reference_in_receipt:
                reference = line.product.reference

            _lines.append({
                'code': code,
                'reference': reference,
                'name': line.product.short_name or line.product.name,
                'unit_price_w_tax': line.unit_price_w_tax,
                'amount_w_tax': line.amount_w_tax,
                'quantity': line.quantity,
                'taxes': [],
                'discount': line.discount_rate,
                'auth_discount': line.auth_discount.name if line.auth_discount else '',
                'parent': line.parent.id if line.parent else None,
            })

        if hasattr(sale, 'salesman') and sale.salesman:
            salesman = sale.salesman.party.name

        party_id_number = party.id_number or ''
        if hasattr(party, 'id_number_full'):
            party_id_number = party.id_number_full
        if party.addresses:
            party_address = party.addresses[0].street

        party_phone = party.phone or party.mobile or ''

        payments = [{
            'name': p.statement.journal.name,
            'amount': p.amount + getattr(p, 'extra_amount', 0) or 0,
            'voucher': p.number or '',
        } for p in sale.payments]

        if sale.delivery_party:
            delivery_party = sale.delivery_party.rec_name

        courtesy = sale.courtesy
        total_discount = 0
        if courtesy:
            total_discount = sum(float(ln.product.get_sale_price_taxed(value=ln.base_price, uom=ln.unit)) * float(ln.quantity) for ln in sale.lines)

        sale_data = {
            'short_invoice': short_invoice,
            'party': party.name,
            'party_id_number': party_id_number,
            'party_address': party_address,
            'party_phone': party_phone,
            'salesman': salesman,
            'agent': sale.agent.rec_name if sale.agent else None,
            'number': number,
            'order': sale.number or '',
            'date': sale_date,
            'untaxed_amount': sale.untaxed_amount,
            'tax_amount': sale.tax_amount,
            'total_amount': sale.total_amount,
            'discount': sale.total_discount,
            'paid_amount': sale.paid_amount,
            'cash_received': sale.cash_received or _ZERO,
            'taxes': sale.process_taxes(),
            'lines': _lines,
            'num_products': str(len(_lines)),
            'turn': sale.turn,
            'change': sale.change or _ZERO,
            'residual_amount': sale.residual_amount,
            'position': sale.position or '',
            'create_date': str(create_date),
            'payment_method': sale.payment_method,
            'comment': sale.comment,
            'description': sale.description,
            'pos_notes': sale.pos_notes,
            'tip': sale_tip,
            'state': sale.state,
            'kind': sale.kind,
            'kind_string': sale.kind_string,
            'invoice_type': sale.invoice_type,
            'qr_code': qr_code,
            'cufe': cufe,
            'invoice_time': sale_time,
            'delivery_party': delivery_party,
            'payments': payments,
            'payment_term': payment_term,
            'auth_kind': auth_kind,
            'tip_amount': sale.tip_amount,
            'delivery_amount': sale.delivery_amount,
            'courtesy': courtesy,
            'total_discount': total_discount,
        }
        if hasattr(sale, 'source'):
            sale_data['source'] = sale.source.name if sale.source else ''
        if hasattr(sale, 'channel'):
            sale_data['channel'] = sale.channel.rec_name if sale.channel else ''
        if hasattr(sale, 'kind_string'):
            sale_data['kind_string'] = sale.kind_string
        if hasattr(sale, 'table_assigned'):
            sale_data['table_assigned'] = sale.table_assigned.name if sale.table_assigned else ''

        sale_data['consumer'] = None
        if hasattr(sale, 'consumer') and sale.consumer:
            sale_data['consumer'] = {
                'consumer_name': sale.consumer.name,
                'consumer_phone': sale.consumer.phone,
                'consumer_address': sale.consumer.address,
            }
        return sale_data

    def process_taxes(self):
        Tax = Pool().get('account.tax')
        sale_taxes = {}
        sale_date = self.sale_date
        for line in self.lines:
            if not line.taxes:
                continue
            for tax in line.taxes:
                if hasattr(tax, 'not_printable') and tax.not_printable:
                    continue
                values = Tax.compute([tax], line.unit_price, line.quantity, sale_date)
                if not values:
                    continue
                values = values[0]
                tax_id = tax.id
                if tax_id not in sale_taxes:
                    if tax.name:
                        name = tax.name[:8]

                    sale_taxes[tax_id] = {
                        'seq': tax.sequence or '0',
                        'name': name,
                        'base': _ZERO,
                        'tax': _ZERO,
                        'total': _ZERO,
                    }
                if tax.type == 'fixed' and line.product.extra_tax:
                    val = int(line.product.extra_tax) * int(line.quantity)
                    values['amount'] = val
                sale_taxes[tax_id]['tax'] += values['amount']
                sale_taxes[tax_id]['base'] += values['base']
        return sale_taxes

    @classmethod
    def to_quote(cls, args, context={}):
        sale = cls(args['sale_id'])
        cls.quote([sale])
        cls.store_cache([sale])
        cls.add_additional_info(sale)
        return {'number': sale.number, 'state': sale.state}

    @classmethod
    def _delete_stock_moves(cls, sale):
        # Must delete all stock moves if exists
        cursor = Transaction().connection.cursor()
        stock_move = Table('stock_move')
        for move in sale.moves:
            cursor.execute(*stock_move.delete(
                where=stock_move.id == move.id),
            )
        sale.shipment_state = 'none'
        sale.save()

    @classmethod
    def _cancel_stock_moves(cls, sale):
        # Must cancel all stock moves if exists
        cursor = Transaction().connection.cursor()
        stock_move = Table('stock_move')
        for move in sale.moves:
            query = stock_move.update(
                columns=[stock_move.state],
                values=['cancel'],
                where=stock_move.id == move.id)
            cursor.execute(*query)

    @classmethod
    def to_draft(cls, args, context=None):
        sale = cls(args['sale_id'])
        if sale.invoices:
            return
        cls.write([sale], {'state': 'draft'})
        cls._delete_stock_moves(sale)

    @classmethod
    def set_sale_number(cls, args):
        sale = cls(args['sale_id'])
        cls.set_number([sale])
        return sale.number

    @classmethod
    def check_state(cls, sale):
        res = {'result': 'ok'}
        if sale.state not in ['draft', 'quotation', 'processing']:
            return {'result': 'sale_closed'}
        return res

    # @classmethod
    # def find(cls, dom, order=[]):
    #     pool = Pool()
    #     SaleLine = pool.get('sale.line')
    #     StatementLine = pool.get('account.statement.line')
    #     fields_names = [
    #         'id', 'state', 'number', 'party', 'party.name', 'invoice_number',
    #         'position', 'payment_term', 'payment_term.name', 'salesman',
    #         'salesman.party.name', 'delivery_charge', 'invoice_type',
    #     ]
    #     if hasattr(cls, 'agent'):
    #         fields_names.extend(['agent', 'agent.party.name', 'commission'])

    #     sales = cls.search_read(dom, fields_names=fields_names, order=order)
    #     sales_ids = [s['id'] for s in sales]
    #     if len(sales_ids) == 1:
    #         sale = sales[0]
    #         lines = SaleLine.search([
    #             ('sale', '=', sales_ids[0]),
    #         ])
    #         sale['lines'] = [l._get_line_pos() for l in lines]

    #         pay_fields_names = ['id', 'voucher_number', 'amount']
    #         sale['payments'] = StatementLine.search_read([
    #             ('sale', '=', sales_ids[0]),
    #         ], fields_names=pay_fields_names)
    #         sales = [sale]
    #     return sales

    @classmethod
    def new_sale(cls, data, context=None):
        pool = Pool()
        Statement = pool.get('account.statement')
        SaleShop = pool.get('sale.shop')
        config = pool.get('sale.configuration')(1)
        Date = pool.get('ir.date')
        statements = Statement.search([
            ('state', '=', 'draft'),
            ('sale_device', '=', data['sale_device']),
        ])
        data['sale_date'] = Date.today()
        if statements:
            data['turn'] = statements[0].turn
            if config.invoice_date_from_statement:
                data['sale_date'] = statements[0].date

        shop = SaleShop(data['shop'])
        if shop.self_pick_up:
            data['self_pick_up'] = shop.self_pick_up

        sale, = cls.create([data])
        party_name = sale.party.name
        payment_type_name = sale.payment_term.name

        res = {
            'id': sale.id,
            'shop': data['shop'],
            'party': {
                'id': data['party'],
                'name': party_name,
                'rec_name': party_name,
            },
            'sale_device': data['sale_device'],
            'payment_term': {
                'id': data['payment_term'],
                'name': payment_type_name,
                'rec_name': payment_type_name,
            },
            'number': None,
            'sale_date': sale.sale_date,
            'invoice_type': data['invoice_type'],
            'state': 'draft',
            'turn': sale.turn,
            'kind': data['kind'],
            'payment_method': 'cash',
        }
        if sale.agent:
            agent_name = sale.agent.rec_name
            res['agent'] = {
                'id': sale.agent.id,
                'rec_name': agent_name,
                'name': agent_name,
            }
        return res

    @classmethod
    def update_delivery_party(cls, args, context=None):
        id_ = args['id']
        data = args['data']
        Party = Pool().get('party.party')
        DeliveryParty = Pool().get('sale.delivery_party')
        delivery_men = DeliveryParty(id_)
        if data.get('party'):
            to_update_party = {}
            party = delivery_men.party
            if data.get('phone'):
                to_update_party['contact_mechanisms'] = [
                    ('create', [
                        {'type': 'phone', 'value': data['phone']},
                    ]),
                ]
                del data['phone']
            Party.write([party], to_update_party)
            del data['party']
        if data.get('id_number'):
            del data['id_number']
        DeliveryParty.write([delivery_men], data)
        return {
                'id': delivery_men.id,
                'active': delivery_men.active,
                'party': {'id_number': party.id_number, 'phone': party.phone},
                'number_plate': delivery_men.number_plate,
                'rec_name': delivery_men.rec_name,
                'type_vehicle': delivery_men.type_vehicle,
            }

    @classmethod
    def create_delivery_party(cls, args, context=None):
        data = args['data']
        if not data.get('party'):
            return {}
        shop_id = args['shop_id']
        Party = Pool().get('party.party')
        ShopDeliveryParty = Pool().get('sale.shop.delivery_party')
        DeliveryParty = Pool().get('sale.delivery_party')
        to_create_party = {
            'name': data['party'],
            'id_number': data['id_number'],
            'type_document': '13',
        }
        if data.get('phone'):
            to_create_party['contact_mechanisms'] = [
                ('create', [
                    {'type': 'phone', 'value': data['phone']},
                ]),
            ]
            del data['phone']
            del data['id_number']
        party, = Party.create([to_create_party])
        data['party'] = party.id
        dparty, = DeliveryParty.create([data])
        ShopDeliveryParty.create([{
            'shop': shop_id,
            'delivery_men': dparty.id,
        }])
        return {
            'id': dparty.id,
            'active': dparty.active,
            'party': {'id_number': party.id_number, 'phone': party.phone},
            'number_plate': dparty.number_plate,
            'rec_name': dparty.rec_name,
            'type_vehicle': dparty.type_vehicle,
        }

    @classmethod
    def get_product_printers(cls, shop_id):
        Printer = Pool().get('sale.pos_printer')
        fields_printer = [
            "shop", "interface", "host", "port",
            "row_characters", "categories.sequence",
            "categories.category.name", "name", "sale_device"]
        printers = Printer.search_read(
            [('shop', '=', shop_id)],
            fields_names=fields_printer)
        printers_ = {}
        for p in printers:
            printer = {
                'name': p['name'],
                'interface': p['interface'],
                'host': p['host'],
                'port': p['port'],
                'sale_device': p['sale_device'],
                'row_characters': p['row_characters'],
                'categories': None,
            }
            if len(p.get('categories.')) > 0:
                printer['lines'] = {c['sequence']: {'name': c['category.']['name'], 'lines': []} for c in p['categories.']}
                printer['categories'] = {c['category.']['id']: c['sequence'] for c in p['categories.']}
            printers_[p['id']] = printer

        cursor = Transaction().connection.cursor()
        query = """
            SELECT
                p.id as product,
                array_agg(tp.printer) as printer,
                array_agg(c.category) as category
            FROM
                product_template_sale_pos_printer_rel as tp
            JOIN
                product_template t on tp.template=t.id
            JOIN
                product_product as p on p.template=t.id
            JOIN
                sale_pos_printer as a on a.id=tp.printer
            LEFT JOIN
                "product_template-product_category" as c on c.template = t.id
            WHERE
                a.shop = %s
            GROUP BY
                p.id;""" % (shop_id)
        cursor.execute(query)
        _records = cursor.fetchall()
        products = {product: [set(printer), set(category)] for product, printer, category in _records}
        return printers_, products

    @classmethod
    def generate_shipment(cls, args, context=None):
        sale_id = args['sale_id']
        sales = cls.search([('id', '=', sale_id)])
        cls.do_stock_moves(sales)

    @classmethod
    def duplicate_sale(cls, args, context=None):
        sale_id = args['sale_id']
        sale, = cls.search([('id', '=', sale_id)])
        new_sale, = cls.copy([sale])
        new_sale.sale_date = date.today()
        cls.save([new_sale])
        cls.set_number([new_sale])
        return {'sale_id': new_sale.id}

    @classmethod
    def faster_close_statement(cls, args, context=None):
        if not args.get('device'):
            return {'result': False}
        pool = Pool()
        Statement = pool.get('account.statement')
        Device = pool.get('sale.device')
        device = Device(args['device'])
        money = args.get('money')
        if not money:
            money = args['data']

        journals = [j.id for j in device.journals]
        statements = Statement.search([
                ('journal', 'in', journals),
                ('sale_device', '=', device.id),
                ('state', '=', 'draft'),
            ], order=[('create_date', 'ASC')],
        )
        cashier = args.get('cashier', None)
        date_report = None
        for statement in statements:
            date_report = statement.date
            end_balance_list = [statement.start_balance]
            for line in statement.lines:
                end_balance_list.append(line.amount)
            to_update = {
                'end_balance': sum(end_balance_list),
                'cashier': cashier,
                'validation_date': datetime.now(),
            }

            if statement.journal.kind == 'cash':
                money_count = [{'bill': m[0], 'quantity': m[1]} for m in money]
                if money_count:
                    to_update['count_money'] = [('create', money_count)]
            Statement.write([statement], to_update)

        if statements:
            Statement.validate_statement(statements)
        if args.get('send_mail'):
            values = {
                'shop': device.shop.id,
                'company': device.company.id,
                'date_report': date_report,
                'turn': statements[0].turn,
                }
            cls.send_report_mail(data=values)

        return {'result': True}

    @classmethod
    def send_report_mail(cls, data):
        pool = Pool()
        reports = {
            'sale_pos.shop_daily_category_report': 'category',
            'sale_pos.shop_daily_summary_report': 'summary',
            'sale_pos.sale_by_kind_report': 'kind',
            'sale_pos_frontend.sale_square_box_report': 'box',
        }
        Shop = Pool().get("sale.shop")
        shop = Shop(data['shop'])
        date_report = data['date_report']
        turn = data.get('turn')
        date = datetime.strptime(date_report, '%Y-%m-%d') if isinstance(date_report, str) else date_report
        attachments = []
        template = shop.email_template
        if template and shop.reports:
            for report in shop.reports:
                Report = pool.get(report, type='report')
                _report = reports[report]

                if _report in ('category', 'summary'):
                    data_report = {
                        'sale_date': date,
                        'end_date': None,
                        'shop': data['shop'],
                        'company': data['company'],
                        'early_morning_included': True,
                        }
                elif 'kind':
                    data_report = {
                        'start_date': date,
                        'end_date': date,
                        'shop': data['shop'],
                        'company': data['company'],
                        'including_tax': True,
                        'detailed': False,
                        'start_time': None,
                        'end_time': None,
                        }
                elif 'box':
                    data_report = {
                        'date': date,
                        'shop': data['shop'],
                        'company': data['company'],
                        'turn': turn,
                        }
                oext, content, action, filename = Report.execute([], data_report)
                attach = {
                        'attachment': content,
                        'file_name': filename,
                        'extension': oext,
                    }
                attachments.append(attach)
            template.send(
                template, None, None, attach=True, attachments=attachments)

    @classmethod
    def faster_open_statement(cls, args, context=None):
        # Deprecation
        print("DEPRECATION WARNING: Use instead do_open_statement from account_statement_co!!!")
        if not args.get('device'):
            return {'result': False}
        pool = Pool()
        Statement = pool.get('account.statement')
        Device = pool.get('sale.device')
        Invoice = pool.get('account.invoice')
        Date = pool.get('ir.date')
        Sale = pool.get('sale.sale')
        today = Date.today()
        device = Device(args['device'])
        start_balance = args.get('start_balance')
        if not start_balance:
            start_balance = args.get('total_money')
        journals = [j.id for j in device.journals]
        statements = Statement.search([
                ('journal', 'in', journals),
                ('sale_device', '=', device.id),
            ], order=[('date', 'ASC')])
        journals_of_draft_statements = [s.journal for s in statements
                                        if s.state == 'draft']
        vlist = []
        new_turn = 0
        for journal in device.journals:
            statements_today = Statement.search([
                ('journal', '=', journal.id),
                ('date', '=', today),
                ('sale_device', '=', device.id),
            ])
            turn = len(statements_today) + 1
            new_turn = turn
            if journal not in journals_of_draft_statements:
                values = {
                    'name': '%s - %s' % (device.rec_name, journal.rec_name),
                    'journal': journal.id,
                    'company': device.shop.company.id,
                    'start_balance': journal.default_start_balance or Decimal('0'),
                    'end_balance': Decimal('0.0'),
                    'turn': turn,
                    'sale_device': device.id,
                }
                if journal.kind == 'cash' and start_balance:
                    values['start_balance'] = Decimal(start_balance)
                vlist.append(values)
        Statement.create(vlist)
        sales = Sale.search([
            ('sale_device', '=', device.id),
            ('sale_date', '=', today),
            ('payment_term.payment_type', '=', '1'),
            ('payments', '=', None),
            ('state', '!=', 'transferred'),
        ])
        invoices = [s.invoices[0] for s in sales if s.invoices]
        write_ = {'turn': new_turn}
        Sale.write(sales, write_)
        Invoice.write(invoices, write_)
        return {'result': True}

    @classmethod
    def faster_add_product(cls, args):
        pool = Pool()
        Shop = Pool().get('sale.shop')
        Sale = Pool().get('sale.sale')
        shop_id = Transaction().context.get('shop')
        shop = Shop(shop_id)
        SaleLine = pool.get('sale.line')
        Product = pool.get('product.product')
        if not args.get('sale_id'):
            return
        sale_id = args['sale_id']
        sale = Sale(sale_id)
        if not sale or sale.state != 'draft':
            raise AddProductError(gettext('sale_pos_frontend.msg_sale_not_draft_for_add_product'))
        product_id = int(args['product_id'])
        qty = args['qty']
        price_list = args.get('price_list', None) or getattr(shop.price_list, 'id', None)
        product = Product(product_id)
        taxes = cls._get_line_taxes(product)
        price_list = int(price_list) if price_list else None
        uom = product.default_uom.id
        add_context = {
             'uom': uom,
             'price_list': price_list,
             'taxes': taxes,
        }
        if args.get('list_price') is not None:
            unit_price = Decimal(str(round(float(args.get('list_price')), 4)))
        else:
            with Transaction().set_context(add_context):
                unit_price = product._get_sale_unit_price(float(qty))
        if unit_price is None:
            unit_price = product.list_price_used
        # if price_list:
        #     pass
            # prices = cls()._get_product_prices(product, qty, sale_id)
            # if prices.get('unit_price'):
            #     unit_price = prices['unit_price']
        unit_price = Decimal(unit_price).quantize(Decimal(str(10.0 ** -4)))
        line_data = {
            'sale': sale_id,
            'type': 'line',
            'product': product_id,
            'description': product.description or product.template.name,
            'unit': uom,
            'unit_price': unit_price,
            'base_price': unit_price,
            'quantity': qty,
            'qty_fraction': '',
            'note': args.get('note', ''),
            'parent': args.get('parent', None),
            'taxes': [('add', taxes)],
        }

        line, = SaleLine.create([line_data])
        res = line._get_line_pos()
        return res

    # @classmethod
    # def get_product_by_categories(cls, args):
    #     time1 = time.time()
    #     context = Transaction().context
    #     pool = Pool()
    #     Category = pool.get('product.category')
    #     Shop = pool.get('sale.shop')
    #     shop, = Shop.search_read(
    #         [('id', '=', context['shop'])],
    #         fields_names=['product_categories']
    #         )
    #     fields_category = [
    #         'id', 'name', 'parent', 'name_icon', 'childs.name',
    #         'childs.id', 'childs.name', 'childs.parent', 'childs.name_icon',
    #         'products.id', 'products.name', 'products.code',
    #         'childs.products.id', 'childs.products.name',
    #         'childs.products.code',
    #         # 'products.list_price',
    #         # 'products.categories',
    #         # 'childs.products.categories'
    #         ]
    #     cats = Category.search_read(
    #         [('id', 'in', shop['product_categories'])],
    #         fields_names=fields_category)
    #     return cats

    @classmethod
    def get_product_by_categories(cls, args):
        context = Transaction().context
        pool = Pool()
        Product = pool.get('product.product')
        Shop = pool.get('sale.shop')
        shop, = Shop.search_read(
            [('id', '=', context['shop'])],
            fields_names=[
                'product_categories.childs.name',
                'product_categories.childs.name_icon',
                'product_categories.childs.parent',
                'product_categories.name',
                'product_categories.parent',
                'product_categories.name_icon',
                ])

        fields_names = [
            'id', 'name', 'code',
            # 'template.list_price', 'template.sale_uom'
            ]
        product_search_read = Product.search_read
        categories = {}
        for c in shop['product_categories.']:
            categories[c['id']] = c
            if c.get('childs.'):
                for child in c['childs.']:
                    products = product_search_read(
                        [('categories', 'in', [child['id']])],
                        fields_names=fields_names)
                    categories[c['id']][child['id']] = child
                    categories[c['id']][child['id']]['products.'] = products
            else:
                products = product_search_read(
                    [('categories', 'in', [c['id']])],
                    fields_names=fields_names)
                categories[c['id']]['products.'] = products

        categories = list(categories.values())
        return categories

    @classmethod
    def add_product(cls, sale, product_id, qty):
        print('Warning: deprecation method [add_product] will be removed!')
        pool = Pool()
        Product = pool.get('product.product')
        SaleLine = pool.get('sale.line')
        product = Product(product_id)
        prices = cls._get_product_prices(product, qty, sale.id)

        unit_price = product.list_price
        if prices.get('unit_price'):
            unit_price = prices['unit_price']

        line, = SaleLine.create([{
            'sale': sale.id,
            'type': 'line',
            'product': product_id,
            'description': product.name,
            'unit': product.default_uom.id,
            'unit_price': unit_price,
            # 'base_price': unit_price,
            'quantity': qty,
            'note': '',
            'taxes': [('add', cls._get_line_taxes(product, sale.party))],
        }])

        sale_line = {
            'id': line.id,
            'unit_price_w_tax': line.unit_price_w_tax,
            'product': line.product.id,
            'quantity': qty,
            'type': 'line',
            'unit_symbol': product.default_uom.symbol,
            'note': '',
        }
        sale.save()
        res = sale._updated_amounts()
        res['sale_line'] = sale_line
        return res

    @classmethod
    def faster_add_payment(cls, args, context=None):
        pool = Pool()
        StatementLine = pool.get('account.statement.line')
        Config = pool.get('sale.configuration')
        Journal = pool.get('account.statement.journal')
        Invoice = pool.get('account.invoice')
        Date = pool.get('ir.date')
        sale_id = args['sale_id']
        journal_id = args['journal_id']
        cash_received = Decimal(args['cash_received'])
        voucher_number = args.get('voucher_number', None)
        context = Transaction().context
        sale_device = context.get('sale_device')
        if args.get('sale_device'):
            sale_device = args.get('sale_device')
        sale = cls(sale_id)
        if not sale.sale_device or sale.sale_device.id != sale_device:
            sale.sale_device = sale_device
            sale.save()
        journal = Journal(journal_id)
        statement_open_id = cls.is_statement_open(
            journal_id, sale.sale_device.id)
        if not statement_open_id:
            return {'msg': 'statement_closed'}
        account_id = sale.party.account_receivable.id
        if args.get('advance'):
            config = Config(1)
            account_id = config.advance_account.id

        cash_received = Decimal(cash_received)
        total_received = sale.paid_amount + cash_received
        if not args.get('reservation'):
            if (sale.net_amount - sale.paid_amount) <= 0:
                return {
                    'id': None,
                    'statement': None,
                    'amount': 0,
                    'residual_amount': 0,
                    'paid_amount': sale.net_amount,
                    'change': 0,
                    'voucher': None,
                    'msg': 'ok',
                }
            if (-1 < cash_received - sale.residual_amount < 1):
                cash_received = sale.residual_amount

            amount_to_statement = sale.residual_amount
            if cash_received < amount_to_statement:
                amount_to_statement = cash_received
            cash_received -= amount_to_statement

            msg = 'missing_money'
            if (-1 < cash_received <= 1):
                msg = 'ok'
                cash_received = 0
        else:
            msg = 'ok'
            amount_to_statement = Decimal(cash_received)
            cash_received -= amount_to_statement
        line = None
        if amount_to_statement:
            to_create = {
                'sale': sale.id,
                'date': Date.today(),
                'statement': statement_open_id,
                'amount': amount_to_statement,
                'party': sale.party.id,
                'account': account_id,
                'description': sale.invoice_number or '',
            }
            if voucher_number:
                to_create['number'] = voucher_number
            line, = StatementLine.create([to_create])
            line.create_move()
        turn = sale.turn
        if line:
            turn = line.statement.turn

        cls.write([sale], {
            'cash_received': total_received,
            'change': cash_received,
            'turn': turn,
            'reservation': args.get('reservation'),
        })
        if sale.invoice:
            invoice = sale.invoice
            value = {'turn': turn}
            if hasattr(invoice, 'payment_means'):
                value['payment_means'] = list(set(getattr(invoice, 'payment_means', []) or [] + [journal.payment_means_code]))
            Invoice.write([invoice], value)
        sale.save()
        return {
            'id': line.id if line else '',
            'statement': line.statement.name if line else '',
            'amount': amount_to_statement,
            'residual_amount': sale.residual_amount,
            'paid_amount': sale.paid_amount,
            'change':  cash_received,
            'voucher': voucher_number,
            'msg': msg,
        }

    @classmethod
    def is_statement_open(cls, journal_id, sale_device_id):
        Statement = Pool().get('account.statement')
        statements = Statement.search_read([
            ('journal', '=', journal_id),
            ('state', '=', 'draft'),
            ('sale_device', '=', sale_device_id),
        ])
        if not statements:
            return False
        return statements[0]['id']

    @classmethod
    def cancel_sale(cls, sales):
        StatementLine = Pool().get('account.statement.line')
        if not sales or sales[0].invoice_type != 'P':
            return
        sale = sales[0]
        if sale.payments:
            StatementLine.delete(sale.payments)

        cls.write([sale], {'state': 'cancelled'})
        if sale.invoices:
            sale.workflow_to_invoice_cancel()
        if sale.moves:
            cls._cancel_stock_moves(sale)

    @classmethod
    def _get_line_taxes(cls, product, party=None):
        taxes_ = []
        taxes_exempt = ['01']
        Shop = Pool().get('sale.shop')
        shop_id = Transaction().context.get('shop')
        shop = Shop(shop_id)
        for tax in product.customer_taxes_used:
            if shop.tax_exempt and tax.classification_tax in taxes_exempt:
                continue
            taxes_.append(tax.id)

        return taxes_

    @classmethod
    def add_additional_info(cls, sale):
        Party = Pool().get('party.party')
        config = Pool().get('sale.configuration')(1)
        values = {}
        if config.tip_product:
            sum_tip = _ZERO
            for line in reversed(sale.lines):
                if line.product.id == config.tip_product.id:
                    sum_tip += line.amount_w_tax
                    break
            values['tip'] = sum_tip

        if not sale.invoice_address:
            invoice_address = Party.address_get(sale.party, type='invoice')
            values['invoice_address'] = invoice_address
        if not sale.shipment_address:
            shipment_address = Party.address_get(sale.party, type='delivery')
            values['shipment_address'] = shipment_address

        cls.set_party_points(sale)
        cls.write([sale], values)

    @classmethod
    def faster_post_invoice(cls, args, context=None):
        sale = cls(args['sale_id'])
        cls.post_invoices(sale)
        try:
            cls.do_stock_moves([sale])
        except Exception as e:
            print(e)
        res = sale.validate_electronic_send()
        return res

    def validate_electronic_send(self):
        if self.invoice_type == '1':
            for inv in self.invoices:
                if inv.electronic_state != 'authorized':
                    return {'result': False}
        return {'result': True}

    @classmethod
    def reconcile_invoice(cls, args, context=None):
        sale = cls(args['sale_id'])
        if sale.shop.workflow_invoice == 'validated':
            return
        cls.do_reconcile([sale])
        cls.update_state([sale])

    def workflow_to_invoice_cancel(self):
        Invoice = Pool().get('account.invoice')
        Move = Pool().get('account.move')
        if self.invoice and self.invoice.state == 'draft':
            Invoice.write([self.invoice], {
                'invoice_date': self.sale_date,
                'state': 'cancelled',
            })
            if self.invoice.move:
                Move.delete([self.invoice.move])

    @classmethod
    def get_product_prices(cls, args, context=None):
        Product = Pool().get('product.product')
        product = Product(args['ids'][0])
        return cls()._get_product_prices(product, args['quantity'], args['sale_id'])

    def _get_product_prices(self, product, quantity, sale_id=None, use_price_list=True):
        # This method is deprecated
        pool = Pool()
        sale, = self.browse([sale_id])
        PriceListLine = pool.get('product.price_list.line')

        # FIXME
        _lists_lines = PriceListLine.search([
            # ('price_list', '=', sale.price_list.id),
            ('product', '=', product.id),
            ('quantity', '<=', int(quantity)),
            ], order=[('quantity', 'DESC')],
        )
        res = {}
        if not product:
            res['unit_price'] = None
            return res

        unit_price = product.list_price
        if sale and _lists_lines and use_price_list:
            _price_list = _lists_lines[0].price_list
            unit_price = product.list_price
            if hasattr(sale, 'price_list') and sale.price_list:
                ctx = {}
                ctx['price_list'] = _price_list
                ctx['sale_date'] = sale.sale_date
                ctx['customer'] = sale.party.id
                ctx['currency'] = sale.currency.id
                with Transaction().set_context(ctx):
                    unit_price = _price_list.compute(product, quantity, product.default_uom, None)
        res['unit_price'] = unit_price
        res['unit_price_w_tax'] = self._get_unit_price_w_tax(
            product, quantity, unit_price,
        )
        return res

    def _get_unit_price_w_tax(self, product, quantity, price):
        # Full price is price_list with taxes
        res = Decimal(0)
        taxes_ = self.get_base_taxes(product, quantity, price)
        if taxes_:
            for tax_ in taxes_:
                res += tax_['base'] + tax_['amount']
        else:
            res = price

        return res

    def get_base_taxes(self, product, quantity, price):
        # Return base and amount concept for line product
        Tax = Pool().get('account.tax')
        taxes_ = []
        if hasattr(product, 'taxes_category'):
            if product.taxes_category:
                taxes = [x for x in product.account_category.customer_taxes]
            else:
                taxes = [x for x in product.customer_taxes]
            if taxes:
                taxes_ = Tax.compute(taxes, price, quantity, self.sale.sale_date)
        return taxes_

    @classmethod
    def get_discount_total(cls, args, context=None):
        Sale = Pool().get('sale.sale')
        sale, = Sale.search([
            ('id', '=', args['sale_id']),
        ])
        return sale.get_total_discount()

    @classmethod
    def set_party_points(cls, sale):
        # Values is a tuple (field, value)
        config = Pool().get('sale.configuration')(1)
        points = None
        if hasattr(sale.party, 'points'):
            RewardPoints = Pool().get('party.reward_points')
            if hasattr(sale.company, 'money_to_points') and \
                    sale.company.money_to_points not in (None, 0):
                points = int(sale.total_amount / sale.company.money_to_points)
                RewardPoints.create([{
                    'party': sale.party.id,
                    'points': points,
                    'date': sale.sale_date,
                }])
                if points and hasattr(config, 'send_sms_points') \
                        and config.send_sms_points:
                    cls.send_message_points(sale, points)

    @classmethod
    def send_message_points(cls, sale, points):
        if not sale.party.mobile:
            return
        try:
            SmsServer = Pool().get('sms.server')
            Template = Pool().get('sms.template')
            sms_server = SmsServer(1)
            template = Template(1)
            msg = template.content % (
                str(points), str(sale.party.total_points))
            sms_server.send_message(sale.party.mobile, msg)
        except:
            print("Error sending message to %s!" % sale.party.name)

    @classmethod
    def resend_invoice(cls, args, context=None):
        sale, = cls.browse([args['id']])
        res = False
        Sale.post_invoices(sale)
        if sale.invoices:
            invoice = sale.invoices[0]
            if invoice.electronic_state == 'authorized':
                res = True
        return res


class OrderStatusTime(ModelSQL, ModelView):
    "Sale Order Status Time"
    __name__ = 'sale.order_status.time'
    # _rec_name = 'sale'

    sale = fields.Many2One('sale.sale', 'Sale', ondelete='CASCADE')
    created = fields.DateTime('Created')
    requested = fields.DateTime('Requested')
    commanded = fields.DateTime('commanded')
    dispatched = fields.DateTime('Dispatched')
    delivered = fields.DateTime('Delivered')
    rejected = fields.DateTime('Rejected')
    cancelled = fields.DateTime('Cancelled')


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'
    order_sended = fields.Boolean('Order Sended')
    qty_fraction = fields.Char('Qty Fraction')

    @staticmethod
    def default_quantity():
        return 1

    def _get_line_data(self, args, to_update=None):
        res = {
            'quantity': args['quantity'],
            'discount': args['discount'],
            'unit_price_w_tax': self.unit_price_w_tax,
            'amount_w_tax': self.amount_w_tax,
        }
        args.update(res)
        if to_update:
            args.update(to_update)
        return args

    def _get_line_pos(self, to_update=None):
        fields = [
            'product.template.sale_price_w_tax', 'type',
            'quantity', 'unit_price_w_tax', 'product.description', 'note',
            'description', 'qty_fraction', 'amount_w_tax', 'unit.symbol',
            'product.name', 'product.code', 'unit.digits', 'amount',
            'discount_rate', 'product.sale_price_w_tax', 'product.quantity',
            'order_sended', 'product.sale_price_taxed', 'sale', 'unit_price',
            'create_uid.rec_name', 'parent',
        ]
        line, = self.search_read(['id', '=', self.id], fields_names=fields)
        return line

    @classmethod
    def faster_set_discount(cls, args, context=None):
        SaleConfiguration = Pool().get('sale.configuration')
        line_ids = args['line_ids']
        value = args['value']
        type_ = args.get('type')
        config = SaleConfiguration(1)
        method = config.discount_pos_method
        if type_ == 'fixed':
            method = 'fixed'
        lines = cls.browse(line_ids)
        value = float(value)
        records = []
        lines_to_discount = []
        if method == 'fixed':
            lines = [lines[0]]
        for line in lines:
            price_w_tax = line.product.sale_price_taxed

            if method == 'fixed':
                if line.unit_price <= _ZERO or Decimal(value) > price_w_tax:
                    continue

                pdiscount = line._get_discount_from_subtraction(value)
            else:
                pdiscount = value / 100.0
            if config.min_margin and not line._validate_discount(pdiscount):
                continue

            lines_to_discount.append(line)

        for ld in lines_to_discount:
            if method == 'fixed':
                pdiscount = value
            cls._compute_discount(pdiscount, ld, method)
            line = ld._get_line_pos()

            records.append(line)
        if not records:
            res = {
                'msg': 'discount_not_valid',
                'type': 'warning',
                'records': None,
            }
        else:
            res = {
                'msg': 'ok',
                'type': 'success',
                'records': records,
            }

        return res

    # @classmethod
    # def set_discount(cls, line_ids, value):
    #     print('Warning: deprecation method [set_discount] will be removed!')
    #     SaleConfiguration = Pool().get('sale.configuration')
    #     config = SaleConfiguration(1)
    #     lines = cls.browse(line_ids)
    #     value = float(value)

    #     lines_to_discount = []
    #     method = config.discount_pos_method
    #     if method == 'fixed':
    #         lines = [lines[0]]
    #     for line in lines:
    #         price = line.product.list_price

    #         if method == 'fixed':
    #             if line.unit_price <= _ZERO or Decimal(value) > price:
    #                 continue

    #             pdiscount = line._get_discount_from_subtraction(value)
    #         else:
    #             pdiscount = value / 100.0

    #         if config.min_margin and not line._validate_discount(pdiscount):
    #             continue
    #         lines_to_discount.append(line)

    #     for ld in lines_to_discount:
    #         cls._compute_discount(pdiscount, ld, method)
    #     if lines_to_discount:
    #         return True

    def _get_discount_from_subtraction(self, value):
        """
        Compute percentage discount from subtraction
        """
        # pdiscount = float(value) / float(self.product.list_price)
        pdiscount = float(value) / float(self.product.sale_price_taxed)
        return float(pdiscount)

    def _get_discount_from_value(self, value):
        """
        Compute percentage discount of product for change unit price on POS
        """
        pdiscount = 0
        if self.product.list_price:
            pdiscount = 1 - (float(value) / float(self.product.list_price))
        return float(pdiscount)

    def _validate_discount(self, discount):
        """
        Check if amount discount is allowed
        discount :: percentage of discount
        """
        SaleConfiguration = Pool().get('sale.configuration')
        config = SaleConfiguration(1)
        new_price = float(self.product.list_price) * (1 - discount)
        cost_price = self.product.cost_price
        min_margin = config.min_margin
        if hasattr(self.product, 'min_margin') and self.product.min_margin and \
                self.product.min_margin > 0:
            min_margin = self.product.min_margin
        if cost_price == _ZERO:
            return True
        if hasattr(self.product.template, 'final_cost') and \
                self.product.template.final_cost:
            cost_price = self.product.template.final_cost
        margin = ((new_price / float(cost_price)) - 1) * 100.0

        if min_margin and margin < min_margin:
            return False
        return True

    @classmethod
    def set_quantity(cls, ids, quantity, list_price=None):
        print('Warning: deprecation method [set_quantity] will be removed!')
        line, = cls.browse(ids)
        if list_price:
            prices = line.sale._get_product_prices(
                line.product, float(quantity), line.sale.id)
            if prices.get('unit_price'):
                cls.write([line], {'quantity': quantity,
                                   'unit_price': prices['unit_price']})
        else:
            cls.write([line], {'quantity': quantity})
        return

    @classmethod
    def faster_set_quantity(cls, args, context=None):
        id = args['id']
        quantity = args['quantity']
        use_price_list = args.get('use_price_list', None)
        context = Transaction().context

        line, = cls.browse([id])
        add_context = {
             'uom': line.unit.id,
             'taxes': line.taxes,
        }
        values = {'quantity': quantity}
        if use_price_list is not None:
            prices = line.sale._get_product_prices(line.product,
                float(quantity), line.sale.id, use_price_list)
            if prices.get('unit_price'):
                values.update({'unit_price': prices['unit_price'], 'base_price': prices['unit_price']})
        elif context.get('price_list'):
            with Transaction().set_context(add_context):
                unit_price = line.product._get_sale_unit_price(float(quantity))
                if unit_price:
                    unit_price = Decimal(unit_price).quantize(Decimal(str(10.0 ** -2)))
                    values.update({'unit_price': unit_price, 'base_price': unit_price})
        cls.write([line], values)
        res = line._get_line_pos(values)
        return res

    @classmethod
    def _get_unit_price_with_tax(cls, product, date):
        Tax = Pool().get('account.tax')
        tax_list = Tax.compute(product.customer_taxes_used,
                               product.list_price, 1, date)
        extra_tax = product.extra_tax or 0
        tax_amount = sum([t['amount'] for t in tax_list], Decimal('0.0')) + extra_tax
        res = product.list_price + tax_amount
        return res

    @classmethod
    def _compute_discount(cls, discount, line, type=None):
        sale_date = line.sale.sale_date
        if type == 'fixed':
            unit_price_w_tax = cls._get_unit_price_with_tax(line.product, sale_date)
            unit_price_w_tax -= (Decimal(discount))
            extra_tax = line.product.extra_tax
            if extra_tax:
                unit_price_w_tax -= extra_tax
            if line.taxes:
                pool = Pool()
                Tax = pool.get('account.tax')
                unit_price = Tax.reverse_compute(Decimal(unit_price_w_tax), line.taxes, sale_date)
            else:
                unit_price = Decimal(unit_price_w_tax)

            unit_price = Decimal(unit_price).quantize(Decimal(str(10.0 ** -2)))
            cls.write([line], {'unit_price': unit_price})
            line.on_change_discount_rate()
        else:
            line.discount_rate = Decimal(discount).quantize(Decimal(str(10.0 ** -2)))
            line.on_change_discount_rate()
            line.save()
            # unit_price_w_tax = cls._get_unit_price_with_tax(line.product)
            # unit_price_w_tax -= (Decimal(discount)*unit_price_w_tax)

    @classmethod
    def set_faster_unit_price(cls, args, context=None):
        print('Warning: deprecation method [set_unit_price] will be removed!')
        line, = cls.browse([args['id']])
        value = Decimal(args['value'])
        extra_tax = line.product.extra_tax
        if extra_tax:
            value -= extra_tax
        if line.taxes:
            pool = Pool()
            Tax = pool.get('account.tax')
            unit_price = Tax.reverse_compute(value, line.taxes, line.sale.sale_date)
        else:
            unit_price = value
        unit_price = Decimal(unit_price).quantize(
            Decimal(str(10.0 ** -2)))

        if args['discount'] is False:
            cls.write([line], {'unit_price': unit_price, 'base_price': unit_price})
            line.on_change_discount_rate()
            return line._get_line_pos()
        else:
            pdiscount = line._get_discount_from_value(unit_price)

            if line._validate_discount(pdiscount):
                cls.write([line], {'unit_price': unit_price})
                return line._get_line_pos()
        return False


class SaleFixNumberStart(ModelView):
    "Sale Fix Number Start"
    __name__ = 'sale_pos_frontend.sale_fix_number.start'
    number = fields.Char('New Number', required=True)


class SaleFixNumber(Wizard):
    "Sale Fix Number"
    __name__ = 'sale_pos_frontend.sale_fix_number'
    start = StateView('sale_pos_frontend.sale_fix_number.start',
                      'sale_pos_frontend.sale_fix_number_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Ok', 'accept', 'tryton-ok', default=True),
                          ])
    accept = StateTransition()

    def transition_accept(self):
        sale_sale = Table('sale_sale')
        cursor = Transaction().connection.cursor()
        id_ = Transaction().context['active_id']
        if id_:
            cursor.execute(*sale_sale.update(
                columns=[sale_sale.number],
                values=[self.start.number],
                where=sale_sale.id == id_),
            )
        return 'end'


class SaleSquareBoxStart(ModelView):
    "Sale Square Box Start"
    __name__ = 'sale_pos_frontend.sale_square_box.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    date = fields.Date('Date', required=True)
    shop = fields.Many2One('sale.shop', 'Shop', required=True)
    sale_device = fields.Many2One('sale.device', 'Sale Device', required=True,
        domain=[
            ('shop', '=', Eval('shop')),
        ])
    turn = fields.Selection([
        ('', ''),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
    ], 'Turn')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_shop():
        return Transaction().context.get('shop')

    @staticmethod
    def default_user():
        return Transaction().user

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


class SaleSquareBox(Wizard):
    "Sale Square Box"
    __name__ = 'sale_pos_frontend.sale_square_box'
    start = StateView(
        'sale_pos_frontend.sale_square_box.start',
        'sale_pos_frontend.sale_square_box_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
         ])
    print_ = StateReport('sale_pos_frontend.sale_square_box_report')

    def do_print_(self, action):
        report_context = {
            'company': self.start.company.id,
            'date': self.start.date,
            'shop': self.start.shop.id,
            'device': self.start.sale_device.id,
            'turn': self.start.turn,
        }
        return action, report_context

    def transition_print_(self):
        return 'end'


class SaleSquareBoxReport(Report):
    "Square Box Report"
    __name__ = 'sale_pos_frontend.sale_square_box_report'

    @classmethod
    def __setup__(cls):
        super(SaleSquareBoxReport, cls).__setup__()

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Sale = pool.get('sale.sale')
        Company = pool.get('company.company')
        # Device = pool.get('sale.device')
        company = Company(data['company'])
        Statement = pool.get('account.statement')
        Shop = pool.get('sale.shop')
        User = pool.get('res.user')
        user_id = Transaction().user
        user = User(user_id)
        date_ = data['date']
        if isinstance(data['date'], str):
            year, month, day = data['date'].split('-')
            date_ = date(int(year), int(month), int(day))
        credits = []
        statement_cash = []
        statement_electronic = []
        total_delivery = []
        total_tip = []
        shop_name = Shop(data['shop']).name

        dom_statement = [
            ('date', '=', date_),
        ]
        device_id = data.get('device')
        if not device_id:
            device_id = user.sale_device.id

        dom_statement.append(('sale_device', '=', device_id))

        if data['turn']:
            dom_statement.append(('turn', '=', int(data['turn'])))
        statements = Statement.search(dom_statement)
        expenses = []
        total_expenses = 0
        statement_cash_ = None
        cashier = None
        paid_invoices_ids = []
        additional_incomes = []
        statements_id = []
        sales_reservations = []
        for statement in statements:
            statements_id.append(statement.id)
            kind = statement.journal.kind
            st_amount = sum(l.amount for l in statement.lines)

            if kind == 'cash':
                statement_cash_ = statement
                statement_cash.append(st_amount)
                cashier = statement.cashier.name if statement.cashier else ''
            else:
                statement_electronic.append(st_amount)
            expenses.extend(statement.expenses_daily)
            # paid_invoices_ids = [
            #     stl.invoice.id for stl in statement.lines if stl.invoice
            # ]
            for stl in statement.lines:
                if stl.sale and stl.sale.invoices:
                    invoice = stl.sale.invoices[0]
                    paid_invoices_ids.append(invoice.id)
                elif stl.sale and stl.sale.reservation:
                    sales_reservations.append(stl.sale.id)
            for additional in statement.additional_income:
                additional_incomes.append(additional.amount)

        total_expenses = sum([e.amount for e in expenses])

        # We need two search of invoices: the first one for credit and the
        # second the paids or partial paids

        # Search credit invoices
        dom_invoices = [
            ('company', '=', data['company']),
            ('invoice_date', '=', date_),
            ('type', '=', 'out'),
            ('number', '!=', None),
            ('shop', '=', data['shop']),
            ['OR',
                [
                    ('payment_term.payment_type', '=', '2'),
                    ('state', 'in', ['posted', 'paid', 'validated']),
                ],
                [
                    ('payment_term.payment_type', '=', '1'),
                    ('state', 'in', ['posted', 'validated']),
                ],
            ],
        ]
        if data['turn']:
            dom_invoices.append(('turn', '=', int(data['turn'])))

        if device_id:
            dom_invoices.append(('sale_device', '=', device_id))

        invoices1 = Invoice.search(dom_invoices, order=[('number', 'ASC')])

        # Search paid invoices
        # Maybe it paid different day (at early morning) but in the
        # same statement/turn
        dom_invoices = [
            ('company', '=', data['company']),
            ('id', 'in', paid_invoices_ids),
            ('type', '=', 'out'),
            ('number', '!=', None),
            ('shop', '=', data['shop']),
            ('state', 'in', ['posted', 'paid', 'validated']),
        ]
        # if data['turn']:
        #     dom_invoices.append(('turn', '=', int(data['turn'])))
        invoices2 = Invoice.search(dom_invoices, order=[('number', 'ASC')])

        # Here join all invoices
        invoices = set(invoices1 + invoices2)
        invoices_number = []
        total_invoices_cash = []
        total_invoices_electronic = []
        total_invoices_credit = []
        total_invoices_credit_balance = []
        delivery_cash = []
        tip_cash = []
        delivery_electronic = []
        tip_electronic = []
        total_invoices_amount = []
        total_invoices_transfers = []
        total_invoices_others = []
        for invoice in invoices:
            invoices_number.append(invoice.number)
            cash = 0
            electronic = 0
            transfer = 0
            others = 0
            total_invoices_amount.append(invoice.net_amount)
            if not invoice.sales:
                continue
                # raise ValidationSaleInvoice(
                #     gettext('sale_pos_frontend.msg_invoice_without_sales', s=invoice.number))

            sale = invoice.sales[0]
            tip_amount = sale.tip_amount or 0
            delivery_amount = sale.delivery_amount or 0
            total_delivery.append(delivery_amount)
            total_tip.append(tip_amount)
            # tip_on_cash = False
            # delivery_on_cash = False
            if not sale.payments:
                total_invoices_credit.append(invoice.net_amount)
                credits.append(invoice)
                total_invoices_credit_balance.append(invoice.amount_to_pay)
                print('invoice', invoice.number, invoice.amount_to_pay, invoice.net_amount)
            else:
                invoice_paid_amount = 0
                for p in sale.payments:
                    if p.statement.date != date_:
                        continue
                    if data['turn'] and p.statement.turn != int(data['turn']):
                        continue
                    amount = p.amount
                    if p.statement.id not in statements_id:
                        invoice_paid_amount += amount
                        continue
                    kind = p.statement.journal.kind
                    invoice_paid_amount += amount
                    if kind == 'cash':
                        cash += amount
                        total_invoices_cash.append(amount)
                        # tip_on_cash = True
                        # delivery_on_cash = True
                    else:
                        # tip_on_cash = False
                        # delivery_on_cash = False
                        if kind == 'transfer':
                            total_invoices_transfers.append(amount)
                            transfer += amount
                        elif kind == 'electronic':
                            electronic += amount
                            total_invoices_electronic.append(amount)
                        else:
                            others += amount
                            total_invoices_others.append(amount)
                if invoice.state == 'posted':
                    total_invoices_credit.append(invoice.net_amount)
                    pending = invoice.net_amount - invoice_paid_amount
                    invoice.amount_to_pay = pending
                    credits.append(invoice)
                    total_invoices_credit_balance.append(pending)
                    # delivery_on_cash = False
                    # tip_on_cash = False

            # if tip_amount:
            #     if tip_on_cash:
            #         tip_cash.append(tip_amount)
            #     else:
            #         tip_electronic.append(tip_amount)
            # if delivery_amount:
            #     if delivery_on_cash:
            #         delivery_cash.append(delivery_amount)
            #     else:
            #         delivery_electronic.append(delivery_amount)

        # reservations without invoice
        sales = Sale.search([('id', 'in', sales_reservations)])

        for sale in sales:
            for p in sale.payments:
                amount = p.amount
                if p.statement.date != date_:
                    continue
                if data['turn'] and p.statement.turn != int(data['turn']):
                    continue
                kind = p.statement.journal.kind
                if kind == 'cash':
                    total_invoices_cash.append(amount)
                elif kind == 'transfer':
                    total_invoices_transfers.append(amount)
                elif kind == 'electronic':
                    total_invoices_electronic.append(amount)
                else:
                    total_invoices_others.append(amount)
                total_invoices_amount.append(p.amount)

        # transferreds sales
        dom_sales = [
            ('company', '=', data['company']),
            ('sale_date', '=', date_),
            ('shop', '=', data['shop']),
            ('sale_device', '=', device_id),
            ('state', '=', 'transferred'),
        ]
        if data['turn']:
            dom_sales.append(('turn', '=', int(data['turn'])))
        transfer_sales = Sale.search(dom_sales)

        # courtesy sales
        dom_sales = [
            ('company', '=', data['company']),
            ('sale_date', '=', date_),
            ('shop', '=', data['shop']),
            ('sale_device', '=', device_id),
            ('state', '=', 'done'),
            ('courtesy', '=', True),
        ]
        if data['turn']:
            dom_sales.append(('turn', '=', int(data['turn'])))
        courtesy_sales = Sale.search(dom_sales)
        total_invoices_amount = sum(total_invoices_amount)
        total_invoices_cash = sum(total_invoices_cash)
        start_balance = 0
        end_balance = 0
        total_cash_balance = 0
        total_money = 0
        count_money = []
        if statement_cash_:
            start_balance = statement_cash_.start_balance
            end_balance = start_balance + total_invoices_cash + sum(additional_incomes)
            total_cash_balance = statement_cash_.end_balance
            total_money = statement_cash_.total_money or 0
            count_money = statement_cash_.count_money
        tip_cash = sum(tip_cash)
        delivery_cash = sum(delivery_cash)
        tip_electronic = sum(tip_electronic)
        delivery_electronic = sum(delivery_electronic)
        total_net_cash = end_balance - total_expenses - \
            tip_electronic - delivery_electronic
        report_context['cashier'] = cashier
        report_context['date'] = date_
        report_context['shop'] = shop_name
        report_context['turn'] = data['turn'] or ''
        report_context['company'] = company.party.name
        report_context['user'] = user.name
        report_context['print_date'] = datetime.now()
        report_context['credits'] = credits
        report_context['expenses'] = expenses
        report_context['total_expenses'] = total_expenses
        report_context['statement'] = statement_cash_
        report_context['transfer_sales'] = transfer_sales
        report_context['courtesy_sales'] = courtesy_sales
        report_context['statement_start_balance'] = start_balance
        report_context['statement_end_balance'] = end_balance
        report_context['total_invoices_cash'] = total_invoices_cash
        report_context['additional_incomes'] = sum(additional_incomes)
        report_context['total_invoices_transfers'] = sum(
            total_invoices_transfers)
        report_context['total_invoices_others'] = sum(total_invoices_others)
        report_context['total_invoices_electronic'] = sum(
            total_invoices_electronic)
        report_context['total_invoices_credit'] = sum(total_invoices_credit)
        report_context['total_invoices_amount'] = total_invoices_amount
        report_context['total_invoices_balance'] = total_invoices_amount
        report_context['total_invoices_credit_balance'] = sum(
            total_invoices_credit_balance)
        report_context['total_cash_balance'] = total_cash_balance
        # report_context['delivery_electronic'] = delivery_electronic
        # report_context['delivery_cash'] = delivery_cash
        # report_context['tip_electronic'] = tip_electronic
        report_context['statements'] = statements
        report_context['total_delivery'] = sum(total_delivery)
        report_context['total_tip'] = sum(total_tip)
        report_context['total_net_cash'] = total_net_cash
        report_context['total_square'] = total_money - total_net_cash
        report_context['total_money'] = total_money
        report_context['count_money'] = count_money
        return report_context


class DeliveryTipsStart(ModelView):
    "delivery Tip Start"
    __name__ = 'sale_pos_frontend.delivery_tip.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    shop = fields.Many2One('sale.shop', 'Shop')
    turn = fields.Selection([
        ('', ''),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
    ], 'Turn')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class DeliveryTips(Wizard):
    "delivery Tips"
    __name__ = 'sale_pos_frontend.delivery_tip'
    start = StateView('sale_pos_frontend.delivery_tip.start',
        'sale_pos_frontend.delivery_tip_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateAction('sale_pos_frontend.delivery_tip_report')

    def do_print_(self, action):
        shop_id = None
        if self.start.shop:
            shop_id = self.start.shop.id
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'shop': shop_id,
            'turn': self.start.turn,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class DeliveryTipsReport(Report):
    "Delivery tip Report"
    __name__ = 'sale_pos_frontend.delivery_tip_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Sale = pool.get('sale.sale')
        Company = pool.get('company.company')

        sale_dom = [
            ('sale_date', '>=', data['start_date']),
            ('company', '=', data['company']),
            ('state', 'in', ['done', 'processing']),
        ]
        if data['end_date']:
            sale_dom.append(
                ('sale_date', '<=', data['end_date']),
            )

        if data['shop']:
            sale_dom.append(
                ('shop', '=', data['shop']),
            )

        if data['turn'] not in ('', None):
            sale_dom.append(
                ('turn', '=', data['turn']),
            )

        fields = [
            'total_amount_cache', 'untaxed_amount_cache', 'tip_amount',
            'delivery_amount', 'sale_date', 'agent.rec_name',
            'number', 'state']
        sales = Sale.search_read(sale_dom, fields_names=fields)
        total_amount = sum(s['total_amount_cache'] for s in sales)
        total_untaxed_amount = sum(s['untaxed_amount_cache'] for s in sales)
        total_tip_amount = sum(s['tip_amount'] for s in sales)
        total_delivery_amount = sum(s['delivery_amount'] for s in sales)
        report_context['total_amount'] = total_amount
        report_context['total_tip_amount'] = total_tip_amount
        report_context['total_delivery_amount'] = total_delivery_amount
        report_context['total_untaxed_amount'] = total_untaxed_amount
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        report_context['sales'] = sales
        report_context['turn'] = data['turn']
        report_context['company'] = Company(data['company']).party.name
        return report_context


class PartyConsumer(ModelSQL, ModelView):
    "Party Consumer"
    __name__ = 'party.consumer'
    name = fields.Char('Name', required=True)
    phone = fields.Char('Phone', required=True)
    address = fields.Char('Address', required=True)
    id_number = fields.Char('Id Number')
    birthday = fields.Date('Birthday')
    delivery = fields.Char('Delivery')
    notes = fields.Text('Notes')

    def get_rec_name(self, name=None):
        return self.name + ' | ' + self.phone + ' | ' + self.address

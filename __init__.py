# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool  # noqa: I001
from . import sale
from . import product
from . import statement
from . import configuration
from . import shop
from . import printer
from . import payment_term
from . import delivery
from . import user
from . import party
from . import ir
from . import invoice


def register():
    Pool.register(
        payment_term.PaymentTerm,
        configuration.Configuration,
        user.User,
        user.UserApplication,
        shop.SaleShop,
        sale.Sale,
        sale.OrderStatusTime,
        sale.PartyConsumer,
        sale.SaleLine,
        sale.DeliveryTipsStart,
        sale.SaleFixNumberStart,
        product.Category,
        product.CategoryProduct,
        product.Product,
        product.Template,
        product.ProductPrinter,
        product.AddPrintersStart,
        delivery.DeliveryParty,
        delivery.SaleRelationDeliveryParty,
        delivery.PaySalesDeliveryForm,
        delivery.SaleDetailedDeliveryManStart,
        shop.ShopProductCategory,
        shop.ShopTaxes,
        shop.SaleShopDeliveryMen,
        # shop.SaleDiscont,
        # shop.SaleShopDiscounts,
        printer.POSPrinter,
        printer.POSPrinterCategory,
        sale.SaleSquareBoxStart,
        statement.Statement,
        statement.MultiPaymentDeliveryStart,
        statement.MultiPaymentDeliveryPay,
        party.Party,
        ir.Cron,
        invoice.Invoice,
        module='sale_pos_frontend', type_='model')
    Pool.register(
        statement.StatementForceDraft,
        sale.SaleFixNumber,
        sale.DeliveryTips,
        sale.SaleSquareBox,
        product.UpdateSalePriceWTax,
        product.AddPrinters,
        delivery.PaySalesDelivery,
        delivery.SaleDetailedDeliveryMan,
        statement.MultiPaymentDelivery,
        module='sale_pos_frontend', type_='wizard')
    Pool.register(
        delivery.PaySalesDelivery,
        delivery.SaleDetailedDeliveryManReport,
        sale.SaleSquareBoxReport,
        sale.DeliveryTipsReport,
        module='sale_pos_frontend', type_='report')

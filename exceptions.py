# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.exceptions import UserError
from trytond.model.exceptions import ValidationError


class SaleDeviceError(UserError):
    pass


class StatementDraftError(UserError):
    pass


class DataSaleError(UserError):
    pass


class ValidationSaleInvoice(ValidationError):
    pass


class MissingPermissionPosAccess(ValidationError):
    pass


class AddPaymentError(ValidationError):
    pass


class AddProductError(UserError):
    pass

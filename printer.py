# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval


class POSPrinter(ModelSQL, ModelView):
    "POS Printer"
    __name__ = "sale.pos_printer"
    name = fields.Char('Name', required=True)
    shop = fields.Many2One('sale.shop', 'Shop', required=True)
    sale_device = fields.Many2One('sale.device', 'Sale Device',
        domain=[('shop', '=', Eval('shop'))],
    )
    interface = fields.Selection([
        ('usb', 'Usb'),
        ('network', 'Network'),
        ('ssh', 'SSH'),
        ('cups', 'Cups'),
    ], 'Interface')
    host = fields.Char('Host')
    port = fields.Char('Port')
    row_characters = fields.Integer('Row Characters')
    categories = fields.One2Many('sale.pos_printer.category',
        'printer', 'Categories')

    @classmethod
    def __setup__(cls):
        super(POSPrinter, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))


class POSPrinterCategory(ModelSQL, ModelView):
    "POS Printer Category"
    __name__ = "sale.pos_printer.category"

    printer = fields.Many2One("sale.pos_printer", 'Printer')
    category = fields.Many2One("product.category", 'Category')
    sequence = fields.Char('Secuence')

    @classmethod
    def __setup__(cls):
        super(POSPrinterCategory, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))

# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class PaymentTerm(metaclass=PoolMeta):
    __name__ = 'account.invoice.payment_term'

    @classmethod
    def __setup__(cls):
        super(PaymentTerm, cls).__setup__()
        cls._disp = False

    @classmethod
    def get_payment_term_pos(cls):
        res = {}
        payment_terms = cls.search([
            ('active', '=', True)
        ])
        res = {p.id: {'name': p.name, 'is_credit': p._is_credit()}
            for p in payment_terms}
        return res

    def _is_credit(self):
        res = []
        for line in self.lines:
            res.append(sum([d.months + d.weeks + d.days
                    for d in line.relativedeltas]))
        if sum(res) > 0:
            return True
        return False
